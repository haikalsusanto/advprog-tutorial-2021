package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

@Service
public class PowerClassifierServiceImpl implements PowerClassifierService{

    @Override
    public String classifyPower(int power) {
        if (0 <= power && power <= 20000) {
            return "C class";
        } else if (20000 < power && power <= 100000) {
            return "B class";
        } else if (100000 < power) {
            return "A class";
        }
        return "You are unclassified";
    }
}
